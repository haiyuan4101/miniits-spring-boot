package com.ms.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: wq
 * @Date: 2017/10/16
 * @Time: 22:38
 * <p>
 * Description:
 * ***
 */

@SpringBootApplication
public class MiniitsJpaApp {

    public static void main(String[] args){
        SpringApplication.run(MiniitsJpaApp.class,args);
    }
}
